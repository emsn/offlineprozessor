import os
import argparse
import datetime
import matplotlib.pyplot as     plt
import numpy             as     np
from   pymongo           import MongoClient

parser = argparse.ArgumentParser(description='Process parameters.')
parser.add_argument('CENTER_FREQUENCY',       type=int,                             help='Transmitter Center Frequency'        )
parser.add_argument('SIZE_FREQUENCY',         type=int,                             help='Frequency Delta for scanning'        )
parser.add_argument('BLANK_FREQUENCY',        type=int,                             help='Blank Delta'                         )
parser.add_argument('DURATION_IN_H',          type=int,                             help='Duration in h'                       )
parser.add_argument('BINARISATION_THRESHOLD', type=int,                             help='Amplitude Threshold for binarisation')
parser.add_argument('FILTER_THRESHOLD',       type=int,                             help='Binarization Filter Threshold'       )
parser.add_argument('FILTER_SIZE',            type=int,                             help='Filter Size'                         )
parser.add_argument('START_TO_PROCESS',       type=datetime.datetime.fromisoformat, help='Start time in Iso'                   )
parser.add_argument('EXPORT_DIR',             type=str,                             help='Path to store results'               )
parser.add_argument('DB_PARAM',               type=str,                             help='DB connection string'                )
args = parser.parse_args()

# Calculated constants
START_FREQUENCY   = args.CENTER_FREQUENCY - args.SIZE_FREQUENCY
END_FREQUENCY     = args.CENTER_FREQUENCY + args.SIZE_FREQUENCY
END_TO_PROCESS    = args.START_TO_PROCESS + datetime.timedelta(hours = args.DURATION_IN_H)

client        = MongoClient(args.DB_PARAM)
db_recordings = client.emsn.recordings

print("%d elements in db." % db_recordings.estimated_document_count())

# Make dir
if not os.path.exists(args.EXPORT_DIR):
    os.mkdir(args.EXPORT_DIR)

date_array      = []
amplitude_array = []
frequency_array = []
counter         = 0

for r in db_recordings.find({"date": { "$gt" : args.START_TO_PROCESS, "$lt" : END_TO_PROCESS}}).sort("date", 1):
    s                 = r['samples']
    d                 = r['date']
    start_frequency   = r['frequency_start']
    end_frequency     = r['frequency_end']
    step_frequency    = r['frequency_step']
    current_frequency = start_frequency
    counter           = counter + 1

    for i, n in enumerate(s):
        if type(n) != float or n == float('-inf'):
            s[i] = args.BINARISATION_THRESHOLD - 1

    temp = []
    for e in s:
        current_frequency += step_frequency
        if current_frequency > START_FREQUENCY and current_frequency < END_FREQUENCY:
            if current_frequency < (args.CENTER_FREQUENCY + args.BLANK_FREQUENCY) and current_frequency > (args.CENTER_FREQUENCY - args.BLANK_FREQUENCY):
                temp.append(args.BINARISATION_THRESHOLD - 10)
            else:
                temp.append(e)
            frequency_array.append(current_frequency)

    s = temp
    date_array.append(d)
    amplitude_array.append(s)

print ("Read %d datasets." % counter)
np_amplitudes = np.array(amplitude_array)
np_dates      = np.array(date_array)
extent        = [frequency_array[0], frequency_array[-1], 60 * args.DURATION_IN_H, 0]

plt.figure(figsize = (5, (30 * args.DURATION_IN_H)))
plt.imshow(np_amplitudes, cmap='jet', vmin=np_amplitudes.min(), vmax = np_amplitudes.max(), aspect='auto', extent=extent)
plt.colorbar()
plt.title('Waterfall from %s' % args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), fontsize=8)
plt.savefig("%s/%s-%d-Waterfall.png" % (args.EXPORT_DIR, args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), args.DURATION_IN_H), bbox_inches='tight')
plt.cla()

print ("Waterfall done.")

def simple_threshold(data, threshold=-42):
    return ((data > threshold)).astype("uint8")

binarized = simple_threshold(np_amplitudes, args.BINARISATION_THRESHOLD)

plt.figure(figsize = (5, (30 * args.DURATION_IN_H)))
plt.imshow(binarized, aspect='auto', vmin=0, vmax=1, extent=extent)
plt.title('Binarized from %s' % args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), fontsize=8)
plt.savefig("%s/%s-%d-Binarized.png" % (args.EXPORT_DIR, args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), args.DURATION_IN_H), bbox_inches='tight')
plt.cla()

print ("Binarized done.")

size_x, size_y     = binarized.shape
filtered_binarized = np.zeros([size_x, size_y])

for x in range (1, size_x - 1):
    for y in range(1, size_y - 1):
        m = 0
        for nx in range(-args.FILTER_SIZE, args.FILTER_SIZE + 1):
            for ny in range(-args.FILTER_SIZE, args.FILTER_SIZE + 1):
                m += binarized[x + nx , y + ny]

        if m > args.FILTER_THRESHOLD:
            filtered_binarized[x, y] = 1
        else:
            filtered_binarized[x, y] = 0

plt.figure(figsize = (5, (30 * args.DURATION_IN_H)))
plt.imshow(filtered_binarized, aspect='auto', extent=extent)
plt.title('Filtered Binarized from %s' % args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), fontsize=8)
plt.savefig("%s/%s-%d-Filtered-Binarized.png" % (args.EXPORT_DIR, args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), args.DURATION_IN_H), bbox_inches='tight')
plt.cla()

print ("Filtered Binarized done.")

sum_per_row = np.sum(filtered_binarized, axis = 1)

plt.figure(figsize = ((30 * args.DURATION_IN_H), 5))
plt.plot(date_array, sum_per_row)
plt.title('Histogram from %s' % args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), fontsize=8)
plt.savefig("%s/%s-%d-Histogram.png" % (args.EXPORT_DIR, args.START_TO_PROCESS.strftime("%Y-%m-%d_%H"), args.DURATION_IN_H), bbox_inches='tight')
plt.cla()

print ("Histogram done.")
