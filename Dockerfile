FROM python

ADD requirements.txt requirements.txt
RUN pip install -r   requirements.txt

ADD processing.py processing.py
CMD python processing.py
